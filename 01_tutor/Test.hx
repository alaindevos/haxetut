/**
    Multi-line comments for documentation.
**/

// Single line comment

import Sys.println ;
import sys.io.File.saveContent;
import haxe.Serializer;
import haxe.Unserializer;
import haxe.ds.Vector;
import haxe.ds.GenericStack;
import haxe.ds.Option;



class MyPrint {
    public var sss:String;
    public function new(ss:String){
        sss=ss;
    }//new
    public function myprint():Void {
        trace("Hello World");
    }//myprint
    public function mymatch(s:String):Void {
        var e:EReg =new EReg("^Hello.*","");
        if(e.match(s)){
            println("Matches");
        }
        else{
            println("Does not Match");
        }
    }//mymatch

    public function myother():Void {
        var b:Bool=true;
        b=false;
        var i:Int=5;
        var f:Float=5.0;
        var s:String="astring";
        var c:String ='c';

        var l:List<String> = new List<String>(); //List
        l.add("TestA");
        if (l.first() == null){
            }
        else {
            println(l.first());
        }//if
        switch("MR"){
            case "MR":
                println("MR");
            case "Ms":
                println("Ms");
            default: 
                println("Other");
        }
        var i:Int=0;
        while(i<4){
            println(i);
            i++;
        }
        do {
            Sys.println(i);
            i--;
        } while(i>=0);
        for(i in 0...4){
            Sys.println(i);
        }
        l.add("TestB");
        for(s in l){
            Sys.println(s);
        }

        var scores = new Array(); //Collection
        scores.push(0);
        scores.push(1);
        scores.push(2);
        scores[1]=5;
        scores = [for (i in 0...10) i];
        var sum = 0;
        for (score in scores) {
            sum += score;
        }//for
        println("Sum Array");
        println(sum);

        var vec = new Vector(10); //Fixed length
        for (i in 0...vec.length) {
            vec[i] = i;
        }

        var myStack = new GenericStack<Int>(); //Given type
        for (ii in 0...5)
            myStack.add(ii);
        trace(myStack); // {4, 3, 2, 1, 0}
        trace(myStack.pop()); // 4

        var map1:Map<Int, String> = [1 => "one", 2 => "two"];
        map1[1]="tree";
        if (map1.exists(1)){
            println(map1[1]);
        }

        var oo:Option<Int>=Some(1);
        switch (oo){
            case None:
                println("None");
            case Some(i):
                println(i);
        }

        var user:{age:Int,name:String}={age:52, name:"Alain"};
        println(user.age);
        var sayHello:String->Void;
        sayHello=function(str:String){
            println(str);
        }//sayhello
        sayHello("Eddy");
        var myAdd:Int->Int->Int;
        myAdd=function(a:Int,b:Int):Int {
            return (a+b);
        }//myadd
        println(myAdd(1,2));

        var incMe:Int->Int;
        incMe=function(a:Int):Int {
            return (a+1);
        }
        var myHigher:(Int->Int,Int)->Int;
        function myHigher(f:Int->Int,x):Int{
            return(f(x));
        }
        println(myHigher(incMe,3));

        var f:Float;
        var i:Int=3;
        f=cast(i,Float);
        f=cast i;
    }//myother

    public function isAdult(age:Int):Bool {
        if(age<18){
            return false ;
        }
        return true ;
    }

    public function printName():Void{
        var u:Usery={name:"Alain",age:52};
        println(u.name);
    }

    public function getEnum():Void{
        var e:NameOrAge =Name("Alain");
        switch (e){
            case Name(n):
                println(n);
            case Age(a):
                println(a);
        }
    }
}//MyPrint

typedef Userx={
    name:String,
    age:Int
}//typedef
typedef Usery=Userx;

enum NameOrAge{
    Name(name:String);
    Age(age:Int);
}

class MyProp1 {
    @:isVar
    public  var s(get,set):String; 
    public function new(ss:String){
        s=ss;
    }//new

    public function get_s():String{
        return s;
    }

    public function set_s(ss:String):String{
        s=ss;
        return s;
    }

}

class Test {
    public static function main():Void {
        var name:String=Sys.args()[0];
        trace(name);
        var myprint:MyPrint = new MyPrint("Test1");
        myprint.myprint();
        myprint.mymatch("HelloWorld");
        myprint.myother();
        myprint.printName();
        myprint.getEnum();
        var serializer = new Serializer();
        serializer.serialize("SERIALIZE");
        serializer.serialize(12);
        var s = serializer.toString();
        trace(s);
        final mydata_data = "mydata.data";
        saveContent(mydata_data,s);

        }
}//Main

