package;

import raylib.Types.ShaderImpl;
import openfl.display.Sprite;
import openfl.Lib;
import openfl.text.TextField;
import openfl.display.Shape;

class Main extends Sprite {
    public function new() {
        super();
        var textField:TextField=new TextField();
        textField.background=true;
        textField.text="Hello World";
        addChild(textField);

        var shape:Shape=new Shape();
        shape.graphics.beginfill(0x0000FF);
        shape.graphics.drawRect(100,100,100,100);
        addchild(shape);
    }

}
