package;

class Person {
    public var name:String;
    public var age:Int;
  
    public function new(name:String, age:Int) {
      this.name = name;
      this.age = age;
    }
  
    public function greet() {
      trace("Hello, my name is " + name + " and I am " + age + " years old.");
    }}

class Main {
    public static function main():Void
    {
        var person1 = new Person("Alice", 30);
        person1.greet();
            var person2 = new Person("Bob", 25);
        person2.greet();
    }}
