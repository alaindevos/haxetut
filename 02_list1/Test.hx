/**
    Multi-line comments for documentation.
**/
// Single line comment

import Sys.println ;

class Node<T> {
    public var value: T;
    public var next: Node<T>;
    public function new(value: T) {
        this.value = value;
    }
}

class LinkedList<T> {
    public var head: Node<T>;
    public function new() {
        head = null;
    }
    public function add(value: T): Void {
        var newNode = new Node(value);
        if (head == null) {
            head = newNode;
        } else {
            var current = head;
            while (current.next != null) {
                current = current.next;
            }
            current.next = newNode;  

        }
    }
    public function remove(value: T): Bool {
        if (head == null) {
            return false;
        }
        if (head.value == value) {
            head = head.next;
            return true;
        }
        var current = head;
        while (current.next != null) {
            if (current.next.value == value) {
                current.next = current.next.next;
                return true;
            }
            current = current.next;
        }
        return false;
    }

public  function print(): Void {
    var current = head;
    while (current != null) {
        Sys.println(current.value);
        current = current.next;
        }
    }
}

class Test {
    public static function main():Void {
        var name:String=Sys.args()[0];
        // trace(name);
        var list = new LinkedList<Int>();
        list.add(10);
        list.add(20);
        list.add(30);
        list.print(); // Output: 10, 20, 30

        list.remove(20);
        list.print(); // Output: 10, 30
    }
}//Main

