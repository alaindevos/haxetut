/**
    Multi-line comments for documentation.
**/
// Single line comment

import Sys.println ;


class Test {
    public static function main():Void {
        var name:String=Sys.args()[0];
        // Create a mutable list of integers
        var myList:List<Int> = new List<Int>();
        // Add elements to the list
        myList.add(1);
        myList.add(2);
        myList.add(3);
        // Print the list
        trace(myList);
        // Remove the last element
        myList.pop();
        // Print the modified list
        trace(myList);
        // Get the length of the list
        trace(myList.length);
        // Check if the list is empty
        trace(myList.isEmpty);
    }
}//Main

