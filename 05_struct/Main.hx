package;

typedef Address = {
    street:String,
    city:String,
    zip:Int
}

@:structInit class Person {
    var name:String;
    var age:Int = 30;
    public function new(name:String, age:Int) {
        this.name = name;
        this.age = age;
    }
    public function getname(){
        return this.name;
    }
}

class Main {
    public static function main():Void
    {
        trace("\nStart\n");
        var address1:Address = {
            street: "123 Main St",
            city: "Anytown",
            zip: 12345
        };
        trace('Address: $address1.street \n  $address1.city, $address1.zip \n');

        var p=new Person("Alain",123);
        var n:String=p.getname();
        trace(' $n\n');
        trace('Stop\n');
    }
}
